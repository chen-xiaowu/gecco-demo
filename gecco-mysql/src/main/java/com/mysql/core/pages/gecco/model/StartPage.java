package com.mysql.core.pages.gecco.model;

import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.spider.HtmlBean;

import java.util.List;

/**
 * @author WeiHui-Z
 * @since 2021-11-04 15:57
 */
@Gecco(matchUrl = "http://mysql.taobao.org/monthly/", timeout = 60 * 1000,
		pipelines = {"consolePipeline", "listPipeline"})
public class StartPage implements HtmlBean {

	@HtmlField(cssPath = "a.main")
	private List<com.mysql.core.pages.gecco.model.PageListModel> pageListModels;

	public List<com.mysql.core.pages.gecco.model.PageListModel> getPageListModels() {
		return pageListModels;
	}

	public void setPageListModels(final List<com.mysql.core.pages.gecco.model.PageListModel> pageListModels) {
		this.pageListModels = pageListModels;
	}

}
